import koa from "koa";
import koaViews from "koa-views";
import koaRouter from "koa-router";
import koaCors from "koa-cors";
import koaStatic from "koa-static";
import koaBody from "koa-body";
import koaSend from "koa-send";
import { rootDir, port } from "./config";
import models from "./db";
import url from "url";
import views from "./views";
import api from "./api";
import Session from "koa-session";

const http = require('http');
const https = require('https');
const fs = require('fs');
const { default: enforceHttps } = require('koa-sslify');

const app = new koa();

app.use(koaStatic(`${rootDir}/static`));

app.use(koaViews(`${rootDir}/views`, {
  extension: "pug"
}));

app.use(koaCors());

app.keys = ['some secret hurr'];

const CONFIG = {
  key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  // maxAge: 86400000*180,
  autoCommit: true, /** (boolean) automatically commit headers (default true) */
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
};

app.use(Session(CONFIG, app));

app.use(koaBody({
  multipart: true,
  formLimit: "500m",
  formidable: {
    maxFieldsSize: 1024 * 1024 * 1024,
    uploadDir: `${process.cwd()}/static/upload`
  }
}))

app.use(( ctx, next ) => {
  ctx.models = models;
  return next();
})

app.use(views);

app.use( async ( ctx, next ) => {
  console.log(ctx.path)
  // ctx.set("header","Access-Control-Allow-Origin", "*");
  // ctx.set("header","Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  await next();

  if(ctx.path.indexOf("api") == -1 && ctx.body === undefined) {
    console.log("render web", ctx.path);
    try {

      await koaSend(ctx, `/public${ctx.path}`);

      // if(ctx.path.indexOf("pki-validation") > -1){
      //   console.log(ctx.path);
      //   await koaSend(ctx, `/build/starfield.html`);
      //   // await koaSend(ctx, `.well-known/pki-validation/starfield.html`);
      // }
      // else {
      //   await koaSend(ctx, `/build${ctx.path}`, {
      //     index: "index.html"
      //   });
      // }

      // var {setting}=await ctx.models.admin.findOne({});
      // if (setting==0){
      //   ctx.redirect("https://s3-ap-northeast-1.amazonaws.com/fanni/other/sys_shut_down.png");
      // }
      // else {
      //   await koaSend(ctx, `/build${ctx.path}`, {
      //     index: "index.html"
      //   });
      // }
    } catch ( err ) {
      console.log(err.message)
      // await koaSend(ctx, `/views/login`);
    }
  }
})

app.use(api);


app.use(enforceHttps({
    port: 4099
}));


var options = {
    key: fs.readFileSync(`${__dirname}/ssl/box/server.key`),
    cert: fs.readFileSync(`${__dirname}/ssl/box/28c9ff0a3684a9d7.crt`)
  // cert: fs.readFileSync(`${__dirname}/ssl/box/a23458643dda5faf.crt`)
}

// start the server
http.createServer(app.callback()).listen(port);
https.createServer(options, app.callback()).listen(4099);
