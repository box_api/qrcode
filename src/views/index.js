import koaRouter from "koa-router";
import scanRoutes from "./scan";
import qrscanRoutes from "./qrscan";
import qrcodeRoutes from "./qrcode";
import booklistRoutes from "./booklist";
import videolistRoutes from "./videolist";
import videocontentRoutes from "./videocontent";
import videocontentlistRoutes from "./videocontentlist";
import loginRoutes from "./login";
import resetpasswordRoutes from "./resetpassword";

let router = new koaRouter();

const render = async (ctx, next) => {
  // await next();
  // ctx.body = {
  //   title: "導覽類別管理"
  // }
  // await ctx.render("index", ctx.body);
}

router.get("/", render);

// router.get("/index", render);
//
router.use("/scan",scanRoutes);
router.use("/qrscan",qrscanRoutes);
router.use("/qrcode",qrcodeRoutes);
router.use("/booklist",booklistRoutes);
router.use("/videolist",videolistRoutes);
router.use("/videocontent",videocontentRoutes);
router.use("/videocontentlist",videocontentlistRoutes);
//
router.use("/login", loginRoutes);

router.use("/resetpassword", resetpasswordRoutes);

export default router.routes();
