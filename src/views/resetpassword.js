import koaRouter from "koa-router";
import R from "ramda";

let router = new koaRouter();

const render = async (ctx, next) => {

  ctx.body = {
    title: "首頁版面管理",
    // banners,
    // panelItems: R.slice(0, 4, blocks),
    // recommendItems: R.slice(4, 5, blocks)
  }
  await ctx.render("resetpassword", ctx.body);
}

router.get("/", render);


export default router.routes();