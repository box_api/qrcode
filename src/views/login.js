import koaRouter from "koa-router";
import R from "ramda";

let router = new koaRouter();

const render = async (ctx, next) => {

  ctx.body = {
    title: "管理員登入",
  }
  await ctx.render("login", ctx.body);
}

router.get("/", render);

export default router.routes();