import koaRouter from "koa-router";
import R from "ramda";

let router = new koaRouter();

const render = async (ctx, next) => {

  const { homeBanner, homeBlock } = ctx.models;
  // const banners = await homeBanner.find({}).sort({ _id: 1 });
  // const blocks = await homeBlock.find({}).sort({_id: 1 }) ;

  ctx.body = {
    title: "首頁版面管理",
    // banners,
    // panelItems: R.slice(0, 4, blocks),
    // recommendItems: R.slice(4, 5, blocks)
  }
  await ctx.render("home/index", ctx.body);
}

router.get("/", render);

router.get("/index", render);



export default router.routes();
