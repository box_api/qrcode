import koaRouter from "koa-router";
import R from "ramda";

let router = new koaRouter();

const render = async (ctx, next) => {
    ctx.body = {
        title: "首頁版面管理",
    }
    await ctx.render("booklist", ctx.body);
}

router.get("/", render);

export default router.routes();