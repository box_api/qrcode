import Sequelize from 'sequelize';
import config from './dbconfig';

//正式
// {
//   "db": {
//   "name": "phoneDB",
//     "username": "phoneadmin",
//     "password": "admin1130",
//     "options": {
//     "host":"phonedb.c5qbkfta5l44.ap-northeast-1.rds.amazonaws.com",
//       "port": 3306,
//       "dialect": "mysql",
//       "logging": false
//   }
// }
// }
//測試
// {
//   "db": {
//   "name": "phoneDB",
//     "username": "mytest",
//     "password": "mytest",
//     "options": {
//     "host":"114.33.155.37",
//       "port": 3306,
//       "dialect": "mysql",
//       "logging": false
//   }
// }
// }
const sequelize = new Sequelize(
  config.db.name,
  config.db.username,
  config.db.password, {
    ...config.db.options,
    define: {
    },
  }
);
const Admin= sequelize.define("Admin", {
  Account: {type:Sequelize.STRING,unique:true},
  Password: {type:Sequelize.STRING},
  Token: {type:Sequelize.STRING,},
  Type: {type:Sequelize.STRING(1)} //0:最高權限 1:不可增加管理者
},{charset: 'utf8',collate: 'utf8_unicode_ci'});

const Book= sequelize.define("Book", {
  code:  {type:Sequelize.STRING},
  title: {type:Sequelize.STRING},
  brief: {type:Sequelize.STRING},
},{charset: 'utf8',collate: 'utf8_unicode_ci'});

const Media= sequelize.define("Media", {
    bookid:{type:Sequelize.INTEGER},
    pagenum:{type:Sequelize.INTEGER},
    position:  {type:Sequelize.STRING},
    filename: {type:Sequelize.STRING},
    url: {type:Sequelize.STRING},
    brief: {type:Sequelize.STRING},
    type: {type:Sequelize.STRING},
    seq:{type:Sequelize.INTEGER},
},{charset: 'utf8',collate: 'utf8_unicode_ci'});


const db = {
  sequelize,
  Admin,
  Book,
  Media,
    // execute: ::sequelize.transaction,
};
export default db;
