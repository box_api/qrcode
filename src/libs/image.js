import loadImage from "blueimp-load-image";

const code = "qwertyuiopasdfghjklzxcvbnm1234567890";
const randomStr = (len) => {
  let result = "";
  for( let i = 0 ; i < len ; i++) {
    const l = Math.floor(Math.random() * code.length);
    result = `${result}${code[l]}`
  }

  return result;
}


function fixRotationOfFile(file) {
  return new Promise((resolve) => {
    loadImage(file, (img) => {
      img.toBlob(
        (blob) => {
          resolve(blob)
        },
        'image/jpeg'
      )
    }, { orientation: true }
    )
  })
}


const fixRotation = async function fixRotation(arrayOfFiles) {
  for (let i = 0; i < arrayOfFiles.length; i++) {
    arrayOfFiles[i] = await fixRotationOfFile(arrayOfFiles[i])
  }
  return arrayOfFiles
}



export const fileToBase64 = file => new Promise((resolve, reject) => {
  let FD = new FileReader();
  FD.addEventListener("load", () => {
    resolve(FD.result);
  }, false)
  FD.readAsDataURL(file);
})


export const rotateImageFile = async (file) => {
  let blobOfArray = await fixRotation([file]);
  const extendName = file.name.split(".").pop();
  const filename = `${new Date().getTime()}-${randomStr(8)}.${extendName}`;
  return new File(blobOfArray, filename, { type: file.type});
}

export const srcToBlob = src => new Promise((resolve) => {

})

export default {
  rotateImageFile,
  fileToBase64
}
