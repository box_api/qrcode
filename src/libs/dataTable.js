import R from "ramda";

const propInt = key => R.pipe(
  R.prop(key),
  data => parseInt(data),
  R.when(
    val => isNaN(val),
    R.always(0)
  )
)

export const parseQuery = R.applySpec({
  pagination: R.applySpec({
    page: propInt("pagination[page]"),
    pages: propInt("pagination[pages]"),
    perpage: propInt("pagination[perpage]"),
    total: propInt("pagination[total]")
  }),
  sort: R.applySpec({
    field: propInt("sort[field]"),
    sort: propInt("sort[desc]")
  }),
  query: R.applySpec({
    q: R.prop("query[q]")
  })
})


export default {
  parseQuery
}
