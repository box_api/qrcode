import models from "../db";
import err from"./errmsg";
import XLSX from "xlsx";
import PubSubClient  from "./pubsubclient";
import md5 from "md5";
import crypto from "crypto";
import moment from "moment";
import iconv from "iconv-lite";
import QRCode from 'qrcode'
import fs from "fs";
//
// export const pubSub =new PubSubClient('ws://13.113.197.241:3001', {
//     connect: true,
//     reconnect: true,
// })



export async function sqlquery(qry) {

  return await models.sequelize.query(qry,{ type: models.sequelize.QueryTypes.SELECT});
}
export async function insquery(qry) {

  return await models.sequelize.query(qry,{ type: models.sequelize.QueryTypes.INSERT});
}
export function arrCatstr(arr,sep) {
  var res="";
  for (var i=0;i<arr.length;++i){
    res+=arr[i].toString()+sep;
  }
  res=res.substring(0,res.length-1);
  return res;
}

export function arrCatdual(arr,fieldid) {
    var res="";
    for (var i=0;i<arr.length;++i){
        res+="select "+arr[i].toString()+" "+fieldid+" from dual union ";
    }
    res=res.substring(0,res.length-6);
    return res;
}

export async function genQrcode(code,text,filename,type) {
  const { registerFont, createCanvas } = require('canvas');
  // registerFont('font/JetBrainsMono-Regular.ttf', { family: 'JetBrains Mono' })
  const canvas = createCanvas(240, 240);
  const res = canvas.getContext('2d');
  const qrcanvas = createCanvas(240, 240);
  const qrres = qrcanvas.getContext('2d');
  // color: {
  //     dark: '#00F',  // Blue dots
  //     light: '#0000' // Transparent background
  // }

  var opts=(type=="P")? { errorCorrectionLevel: 'H' }: { errorCorrectionLevel: 'H',color:{dark:'#00F',light:'#0000'}};
  await QRCode.toCanvas(qrcanvas, code,opts, function (error) {
    if (error) console.error(error); //else  console.log('success!');
    res.drawImage(qrcanvas,0,0,240,240);
    try{

      var out = fs.createWriteStream( filename)
        , stream = canvas.createPNGStream();

      stream.on('data', function(chunk){
        out.write(chunk);
      });

      //畫文字
      res.save();
      res.translate(250, 20);
      res.rotate(Math.PI / 2);
      res.fillStyle =(type=="P")? "#000":"#00F";
      // res.font = '14px Impact';
      // res.bold="bold";
      res.fillText(text, 20, 20);
      res.restore();


      // res.fillStyle = "#000";
      // res.font = '14px "Comic Sans"'
      // // res.bold="bold";
      // res.fillText(text, 20, 20);

    }
    catch (ex){

    }
  })
}

