import dotenv from "dotenv";
dotenv.config();

export const dev = process.env.NODE_ENV != "production";
export const rootDir = process.cwd();
export const port = dev || true ? 4090 : process.env.PORT;

