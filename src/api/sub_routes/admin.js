import { request, summary, query, path, body, tags, responses, formData, middlewares } from "koa-swagger-decorator";
import { common } from "../errors";
import {checkAdminToken, } from "../middlewares/common";
import randtoken from "rand-token";
import { sqlquery,googlePush,googleMultiPush, arrCatstr,genQrcode} from "../../libs/tools";
import moment from "moment";
import Admzip from "adm-zip";
import QRCode from 'qrcode'
import fs from "fs";
import {uploadFile} from "../../libs/s3";

import swagger from "koa-swagger-decorator";

// import ;{ parse } from 'node-html-parser';
// import { $ } from "jquery";
import XLSX from "xlsx";
// import AWS from "aws-sdk";

const tag = tags(["Admin"]);
var OK={message:"OK",code:"0"};
export default class admins {

  @request("post", "/admin/uploadS3")
  @body({
    filename: {type: "string"},
    dir: {type: "string"},
    file: {type: "file"},
  })
  @summary("uploadS3")
  @middlewares([
    checkAdminToken()
  ])
  @tag
  static async uploadS3(ctx) {
    var { file,filename,dir} = ctx.request.body;
    await uploadFile(file,filename,dir);
    ctx.body=OK;
  }

    @request("get", "/admin/getzip/{id}")
    @path({
        id:{type:"string"},
    })
    @summary("getzip")
    @middlewares([
        // checkAdminToken(),
    ])
    @tag
    static async getzip(ctx) {
        const { id } = ctx.validatedParams;
        const azip=new Admzip();
        var dir="./src/qrcode/"+id;
        var data;
        ctx.query.bookid=id;
        var catstr=(id=="")?"":" and a.bookid="+id;
        var sql="SELECT a.id,a.pagenum,a.position,a.filename,a.url,a.brief,a.type,a.bookid,a.createdAt,b.title,a.seq FROM qrcodeDB.Media a left join qrcodeDB.Books b " +
            " on a.bookid=b.id where 1=1 "+catstr;
        var res=await sqlquery(sql);
        if (res.length>0){
            for(var i=0;i<res.length;++i){
                var val=res[i];
                var file=val.pagenum+"-"+val.type+"-"+val.seq+".png";
                console.log(file);
                azip.addLocalFile(dir+"/"+file);
            }
            const downloadName = `${Date.now()}.zip`;
            data = azip.toBuffer();
            ctx.set('Content-Type','application/octet-stream');
            ctx.set('Content-Disposition',`attachment; filename=${downloadName}`);
            ctx.set('Content-Length',data.length);
        }
        else{
            throw common.RECORD_NOT_FOUND()
        }

        ctx.body=data;
    }

  @request("post", "/admin/login")
  @body({
    account:{ type: "string", required:true},
    password:{ type: "string", required:true},
  })
  @summary("管理員登入")
  @middlewares([
    // validatePathId(),
    // checkModelIsExists("fannies")
  ])
  @tag
  static async adminlogin(ctx) {
    const {  account, password} = ctx.request.body;
    const { Admin } = ctx.models;
    const rec = await Admin.findOne({where:{Account: account,Password:password }});

    if (rec==undefined){
      throw common.LOGIN_FAIL();
    }
    var token=randtoken.uid(28);
    rec.Token=token;
    rec.updatedAt=new Date();
    await rec.save();
    ctx.session.adminId=rec.id;
    ctx.body= {status:200, id:rec.id,account:rec.Account};

  }

  @request("post", "/admin/resetpassword")
  @body({
    password:{ type: "string", required:true},
  })
  @summary("管理員變更密碼")
  @middlewares([
    checkAdminToken(),
  ])
  @tag
  static async resetpassword(ctx) {
    const { password} = ctx.request.body;
    const { Admin } = ctx.models;
    const rec = await Admin.findOne({where:{id:ctx.session.adminId}});
    if (rec==undefined){
      throw common.LOGIN_FAIL();
    }
    rec.Password=password;
    await rec.save();
    ctx.body="OK";

  }

  @request("post", "/admin/logout")
  @body({
  })
  @summary("管理員登出")
  @middlewares([
    // validatePathId(),
    // checkModelIsExists("fannies")
  ])
  @tag
  static async adminlogout(ctx) {
    ctx.session.adminId=null;
    ctx.body="OK";
  }

  @request("post", "/admin/addrec")
  @body({
  })
  @summary("add test data")
  @middlewares([
      checkAdminToken(),
  ])
  @tag
  static async addrec(ctx) {
      var {Book,Media}=ctx.models;
      var btn="",btnexp="";
      var arr=[
          {"createdAt":"2020-02-01","title":"英語一","code":"eng-01","brief":"英語一簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-02","title":"英語二","code":"eng-02","brief":"英語二簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-03","title":"英語三","code":"eng-03","brief":"英語三簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-04","title":"英語四","code":"eng-04","brief":"英語四簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-05","title":"英語五","code":"eng-05","brief":"英語五簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-06","title":"英語六","code":"eng-06","brief":"英語六簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-07","title":"英語七","code":"eng-07","brief":"英語七簡介","btn":btn,"btn1":btnexp},
          {"createdAt":"2020-02-08","title":"英語八","code":"eng-08","brief":"英語八簡介","btn":btn,"btn1":btnexp}];
      // await Book.bulkCreate(arr);
      arr=[{"createdAt":"2020-02-01","title":"英語一","pagenum":"11","position":"第一行","type":"家長","brief":"英語影音一簡介"},
          {"createdAt":"2020-02-02","title":"英語2","pagenum":"12","position":"第2行","type":"老師","brief":"英語影音2簡介"},
          {"createdAt":"2020-02-03","title":"英語3","pagenum":"13","position":"第3行","type":"老師","brief":"英語影音3簡介"},
          {"createdAt":"2020-02-04","title":"英語4","pagenum":"14","position":"第4行","type":"老師","brief":"英語影音4簡介"},
          {"createdAt":"2020-02-05","title":"英語5","pagenum":"15","position":"第5行","type":"家長","brief":"英語影音5簡介"},
          {"createdAt":"2020-02-06","title":"英語6","pagenum":"16","position":"上","type":"家長","brief":"英語影音6簡介"},
          {"createdAt":"2020-02-07","title":"英語7","pagenum":"17","position":"中","type":"老師","brief":"英語影音7簡介"},
          {"createdAt":"2020-02-08","title":"英語8","pagenum":"18","position":"下","type":"家長","brief":"英語影音8簡介"}];
      await Media.bulkCreate(arr);
      ctx.body="OK";
  }


  @request("get", "/admin/getbooklist")
  @query({
    // phone:{ type: "string"},
    // pagesize:{ type: "number"},
    // pageno:{ type: "number"},
  })
  @summary("書本管理列表")
  @middlewares([
    checkAdminToken()
  ])
  @tag
  static async getbooklist(ctx) {
    var {Book}=ctx.models;
    // var pageno=(ctx.query.pageno==undefined)?0:ctx.query.pageno;
    // var pagesize=(ctx.query.pagesize==undefined)?20:ctx.query.pagesize;
    // var idx=pageno*pagesize;
    // var sql="SELECT a.id,a.Salesname,a.Phone,a.Level,a.State FROM phoneDB.Sales a where a.Level>1 order by createdAt desc;";
    var allrec=await Book.findAll({});
    ctx.body={...OK,data:allrec};
  }

    @request("get", "/admin/getmedialist")
    @query({
        bookid:{ type: "string"},
    })
    @summary("影音管理列表")
    @middlewares([
        checkAdminToken()
    ])
    @tag
    static async getmedialist(ctx) {
        var {Book}=ctx.models;
        var id=(ctx.query.bookid==undefined||ctx.query.bookid=="")?"":ctx.query.bookid;
        var catstr=(id=="")?"":" and a.bookid="+id;
        var sql="SELECT a.id,a.pagenum,a.position,a.filename,a.url,a.brief,a.type,a.bookid,a.createdAt,b.title,a.seq FROM qrcodeDB.Media a left join qrcodeDB.Books b " +
            " on a.bookid=b.id where 1=1 "+catstr;
        var allrec=await sqlquery(sql);
        ctx.body={...OK,data:allrec};
    }

    @request("post", "/admin/setbook/{id}")
    @body({
        setting:{type:"object"}
    })
    @path({
        id:{type:"number"}
    })
    @summary("儲存書本")
    @middlewares([
        checkAdminToken()
    ])
    @tag
    static async setbook(ctx) {
        var {Book,Media}=ctx.models;
        const { id } = ctx.validatedParams;
        var {setting}=ctx.request.body;
        if (id==0){
          var rec= await  Book.create(setting);
          var dir="./src/qrcode/"+rec.id;
          await !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true });

        }
        else {
          await Book.update(setting,{where:{id:id}});
          //
          //產生qrcode
          var dir="./src/qrcode/"+id;
          const fsExtra = require('fs-extra')
          await fsExtra.emptyDirSync(dir);
          var medias=await Media.findAll({where:{bookid:id}});
          for(var i=0;i<medias.length;++i){
            var val=medias[i];
            var code="https://www.arcterbox.com:4099/qrcode?"+id+"/"+val.pagenum+"/"+val.type+"/"+val.seq;
            var text=setting.code.padEnd(15," ")+val.pagenum;
            // console.log(text)
            var filename=dir+"/"+val.pagenum+"-"+val.type+"-"+val.seq+".png";
            await genQrcode(code,text,filename,val.type);
          }
        }

        ctx.body=OK;
    }

    @request("post", "/admin/setbookmedia/{id}")
    @body({
        medias:{type:"object"}
    })
    @path({
        id:{type:"number"}
    })
    @summary("儲存影音資料")
    @middlewares([
        checkAdminToken()
    ])
    @tag
    static async setbookmedia(ctx) {
        var {Media,Book}=ctx.models;
        const { id } = ctx.validatedParams;
        var {medias}=ctx.request.body;
        var bk=await Book.findOne({where:{id:id}});
        //檢查資料夾
        var dir="./src/qrcode/"+id;
        const fsExtra = require('fs-extra')
        await fsExtra.emptyDirSync(dir)
        // console.log(medias)
        await Media.destroy({where:{bookid:id}});
        if (medias.length>0){
            await Media.bulkCreate(medias);
            //產生qrcode
            for(var i=0;i<medias.length;++i){
                var val=medias[i];
                var code="https://www.arcterbox.com:4099/qrcode?"+id+"/"+val.pagenum+"/"+val.type+"/"+val.seq;
                var text=bk.code.padEnd(15," ")+val.pagenum;
                console.log(text)
                var filename=dir+"/"+val.pagenum+"-"+val.type+"-"+val.seq+".png";
                await genQrcode(code,text,filename,val.type);
            }
        }

        ctx.body=OK;

        // async function genQrcode(code,text,filename,type) {
        //     const { registerFont, createCanvas } = require('canvas');
        //     // registerFont('font/JetBrainsMono-Regular.ttf', { family: 'JetBrains Mono' })
        //     const canvas = createCanvas(240, 240);
        //     const res = canvas.getContext('2d');
        //     const qrcanvas = createCanvas(240, 240);
        //     const qrres = qrcanvas.getContext('2d');
        //     // color: {
        //     //     dark: '#00F',  // Blue dots
        //     //     light: '#0000' // Transparent background
        //     // }
        //
        //     var opts=(type=="P")? { errorCorrectionLevel: 'H' }: { errorCorrectionLevel: 'H',color:{dark:'#00F',light:'#0000'}};
        //     await QRCode.toCanvas(qrcanvas, code,opts, function (error) {
        //         if (error) console.error(error); //else  console.log('success!');
        //         res.drawImage(qrcanvas,0,0,240,240);
        //         try{
        //
        //             var out = fs.createWriteStream( filename)
        //                 , stream = canvas.createPNGStream();
        //
        //             stream.on('data', function(chunk){
        //                 out.write(chunk);
        //             });
        //
        //             //畫文字
        //             res.save();
        //             res.translate(250, 20);
        //             res.rotate(Math.PI / 2);
        //             res.fillStyle =(type=="P")? "#000":"#00F";
        //             // res.font = '14px Impact';
        //             // res.bold="bold";
        //             res.fillText(text, 20, 20);
        //             res.restore();
        //
        //
        //             // res.fillStyle = "#000";
        //             // res.font = '14px "Comic Sans"'
        //             // // res.bold="bold";
        //             // res.fillText(text, 20, 20);
        //
        //         }
        //         catch (ex){
        //
        //         }
        //     })
        // }
    }

    @request("post", "/admin/dltbook/{id}")
    @path({
        id:{type:"number"}
    })
    @summary("刪除書本")
    @middlewares([
        checkAdminToken()
    ])
    @tag
    static async dltbook(ctx) {
        var {Book,Media}=ctx.models;
        const { id } = ctx.validatedParams;
        var dir="./src/qrcode/"+id;
        const fsExtra = require('fs-extra')
        await fsExtra.emptyDirSync(dir)
        await Book.destroy({where:{id:id}});
        await Media.destroy({where:{bookid:id}});

        ctx.body=OK;
    }

    @request("get", "/admin/getmedia/{bookid}/{pageno}/{type}/{seq}")
    @path({
        bookid:{type:"number"},
        pageno:{type:"number"},
        type:{type:"string"},
        seq:{type:"number"},
    })
    @summary("getmedia")
    @middlewares([
        // checkAdminToken()
    ])
    @tag
    static async getmedia(ctx) {
        var {Book,Media}=ctx.models;
        const { bookid,pageno,type,seq } = ctx.validatedParams;
        var sql="SELECT b.title, a.brief,a.url FROM qrcodeDB.Media a left join qrcodeDB.Books b " +
            " on a.bookid=b.id where a.bookid="+bookid+" and a.pagenum="+pageno+" and a.type='"+type+"' and a.seq="+seq;
        var rec=await sqlquery(sql);
        ctx.body= {...OK,data:rec};
    }

    @request("post", "/admin/testqrcode")
    @body({
        code:{type:"string"}
    })
    @summary("testqrcode")
    @middlewares([
        // checkAdminToken()
    ])
    @tag
    static async testqrcode(ctx) {
        const { code } = ctx.request.body;

        // var res=await QRCode.toDataURL(code);
        // ctx.body=res;


        await QRCode.toFile('filename.png', code, {
            // color: {
            //     dark: '#00F',  // Blue dots
            //     light: '#0000' // Transparent background
            // }
        }, function (err) {
            if (err) throw err
            console.log('done')
        })
        ctx.body='OK'
    }


    @request("post", "/admin/testcanvas")
    @body({
        code:{type:"string"}
    })
    @summary("testcanvas")
    @middlewares([
        // checkAdminToken()
    ])
    @tag
    static async testcanvas(ctx) {
        //ref: https://github.com/Automattic/node-canvas/issues/266
        //https://help.accusoft.com/PCC/v8.3/HTML/How%20to%20Install%20Microsoft%20Fonts%20on%20Linux.html
        const { code } = ctx.request.body;

        const { registerFont, createCanvas } = require('canvas')
        // var toSJIS = require('qrcode/helper/to-sjis')
        // registerFont('font/comicbd.ttf', { family: 'Comic Sans' })
        const canvas = createCanvas(240, 240);
        const res = canvas.getContext('2d');
        const qrcanvas = createCanvas(240, 240);
        const qrres = qrcanvas.getContext('2d');
        await QRCode.toCanvas(qrcanvas, 'sample text', function (error) {
            if (error) console.error(error); else  console.log('success!');
            res.drawImage(qrcanvas,5,5,240,240);
            try{

                var out = fs.createWriteStream( 'imagex.png')
                    , stream = canvas.createPNGStream();

                stream.on('data', function(chunk){
                    out.write(chunk);
                });

                //畫文字
                res.save();
                res.translate(240, 20);
                res.rotate(Math.PI / 2);
                res.fillStyle = "#000";
                res.font =  '14px "Comic Sans"'
                res.bold="bold";
                res.fillText(code, 20, 20);
                res.restore();

            }
            catch (ex){

            }
        })


        ctx.body='OK'
    }


}