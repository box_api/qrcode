//import { Types } from "mongoose";
import { common } from "../errors";

export const checkToken  =()=> async (ctx, next) => {
  // console.log(ctx.session.userId,ctx.session.token)
  if (ctx.session.userId==undefined){
    throw common.NOT_AUTH();
  }
  else{
    var mbr=await ctx.models.Sales.findOne({where:{Token:ctx.session.token}})
    if (mbr==undefined){
      throw common.NOT_AUTH();
    }
    if (mbr.State!="Y"){
      throw common.MEMBER_STATE_ERROR();
    }
  }
  return next();
}

export const checkValidUser  =()=> async (ctx, next) => {
  if (ctx.session.userId==undefined){
    throw common.NOT_AUTH();
  }
  else{
    var mbr=await ctx.models.Sales.findOne({where:{id:ctx.session.userId}})
    if (mbr==undefined){
      throw common.NOT_AUTH();
    }
    if (mbr.State!="Y"){
      throw common.MEMBER_STATE_ERROR();
    }
    if (mbr.Token==null){
      throw common.NOT_AUTH();
    }
    if (mbr.Level==1){
      throw common.NOT_AUTHORIZED();
    }
  }
  return next();
}

export const checkShow6  =()=> async (ctx, next) => {
  if (ctx.session.userId==undefined){
    throw common.NOT_AUTH();
  }
  else{
    var mbr=await ctx.models.Sales.findOne({where:{id:ctx.session.userId}})
    if (mbr==undefined){
      throw common.NOT_AUTH();
    }
    if (mbr.State!="Y"){
      throw common.MEMBER_STATE_ERROR();
    }
    if (mbr.Token==null){
      throw common.NOT_AUTH();
    }
    if (mbr.Level==1){
      throw common.NOT_AUTHORIZED();
    }
    if (mbr.Show6!="Y"){
      throw common.NOT_AUTHORIZED();
    }
  }
  return next();
}

export const checkAdminToken  =()=> async (ctx, next) => {

  if (ctx.session.adminId==undefined){
    throw common.ADMIN_NOT_AUTH();
    // ctx.redirect("./login");
  }
  return next();
}

// export const validatePathId = ( key = "_id") => (ctx, next) => {
//   if( !Types.ObjectId.isValid(ctx.validatedParams[key]) ) {
//     throw common.ID_VALIDATOR_ERROR()
//   }
//   return next();
// }


// export const checkModelIsExists = key => async (ctx, next) => {
//   const { _id } = ctx.validatedParams;
//   const isCheck = await ctx.models[key].count({ _id });
//   console.log(key, isCheck);
//   if(isCheck == 0) {
//     if (key=="loginlog"){
//       ctx.redirect("https://s3-ap-northeast-1.amazonaws.com/fanni/other/line_err.png");
//     }
//     else {
//       throw common.MODEL_NOT_FOUND();
//     }
//   }
//
//   return next();
// }


