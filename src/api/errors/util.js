import errors from "http-errors";

export const createErrors = (status, message, code = "000") => props => errors(status, message, { code, ...props });
