import { createErrors } from "./util";
//Object Id Validator
export default {
  ID_VALIDATOR_ERROR: createErrors(400, "功能尚未開放", "001"),
  LOGIN_FAIL: createErrors(400, "登入失敗", "001"),
  NOT_AUTH: createErrors(400, "請登入會員", "002"),
  MEMBER_STATE_ERROR: createErrors(400, "尚未成為正式會員", "002"),
  ADMIN_NOT_AUTH: createErrors(400, "請登入", "002"),
  MODEL_NOT_FOUND: createErrors(400, "資料已被刪除 Data is deleted", "003"),
  DUPLICATE: createErrors(400, "電話號碼重覆", "004"),
  DUPLICATE1: createErrors(400, "業務名稱重覆", "004"),
  NICKNAME_DUPLICATE: createErrors(400, "名稱重覆", "004"),
  PHONE_DUPLICATE: createErrors(400, "電話號碼重覆", "004"),
  RECORD_NOT_FOUND: createErrors(400, "查無資料", "005"),
  RECORD_NOT_FOUND1: createErrors(400, "查無推播資料", "005"),
  REC_DUPLICATE: createErrors(400, "資料重覆", "006"),
  DELMEMBER_ERROR: createErrors(400, "後台建立的會員才可刪除", "006"),
  EMPTY_NOT_ALLOW: createErrors(400, "資料不可空白", "007"),
  TOO_LARGE_ERR: createErrors(400, "資料過大，請縮小查詢範圍", "007"),
  SPACE_NOT_ALLOW: createErrors(400, "帳號不可有空格", "007"),
  UNEXPECTED_ERR: createErrors(400, "Unexpect error", "009"),
  DATE_ERR: createErrors(400, "日期格式錯誤", "009"),
  PARAMETER_ERR: createErrors(400, "輸入參數錯誤", "010"),
  PASSWORD_ERR: createErrors(400, "密碼錯誤", "010"),
  NOT_AUTHORIZED: createErrors(400, "無權限使用此功能", "010"),

  ACCOUNT_EMPTY: createErrors(400, "帳號不可空白", "007"),
  SMS_DUP: createErrors(400, "驗證碼已發送", "010"),
  SMS_ERROR: createErrors(400, "尚未驗證手機", "010"),
  SMSCODE_ERROR: createErrors(400, "驗證碼錯誤", "010"),
  PUSH_ERROR1: createErrors(400, "MismatchSenderId", "010"),
  FORMAL_MEMBER_ERROR: createErrors(400, "正式會員才可執行此功能,請洽系統管理員", "010"),

  ZIPCODE_ERROR: createErrors(400, "郵遞區號錯誤", "010"),

  BUYCREDIT_ERROR1: createErrors(400, "購買額度不可小於1000", "010"),
  BUYCREDIT_ERROR2: createErrors(400, "點數折抵不可超過1000", "010"),
  BUYCREDIT_ERROR3: createErrors(400, "點數不足，無法折抵", "010"),

  QUERY_ERROR1: createErrors(400, "查詢註冊會員只可查詢自己電話", "010"),
  QUERY_ERROR2: createErrors(400, "每天查詢已達5次", "010"),
  QUERY_ERROR3: createErrors(400, "選配方案點數不足", "010"),
  QUERY_ERROR4: createErrors(400, "回報點數不足", "010"),
  QUERY_ERROR5: createErrors(400, "無查看回報紀錄清單權限", "010"),

  REPORT_ERROR1: createErrors(400, "不可重複回報", "010"),
  REPORT_ERROR2: createErrors(400, "沒有回報的權限", "010"),

  UPGRADE_ERROR: createErrors(400, "升級後版本號必須大於目前版本號", "010"),

  SALES_ITEM_ERROR1: createErrors(400, "該商品已停售", "010")
}

