import koaRouter from "koa-router";
import R from "ramda";
import { wrapper } from 'koa-swagger-decorator';
import dataTable from "../libs/dataTable";
import _path from 'path';



let router = new koaRouter({
  prefix: "/api"
});

router.use(async (ctx, next) => {
  try {
    await next();
  } catch ( err ) {
    console.log(err);
    ctx.status = err.status || 400;
    ctx.body = err;
  }
})

wrapper(router);


// swagger docs avaliable at http://localhost:3080/api/swagger-html
router.swagger({

  title: 'Example Server',
  description: 'API DOC',
  version: '1.0.0',

  // [optional] default is root path.
  prefix: '/api',

  // [optional] default is /swagger-html
  swaggerHtmlEndpoint: '/swagger-html',

  // [optional] default is /swagger-json
  swaggerJsonEndpoint: '/swagger-json',

  // [optional] additional options for building swagger doc
  // eg. add api_key as shown below
  swaggerOptions: {
    securityDefinitions: {
      ApiKeyAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization'
      }
    },
  }
});


router.mapDir(_path.resolve(__dirname, './sub_routes'), { recursive: true });

export default router.routes();
