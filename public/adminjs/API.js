var path=window.location.origin+"/";
var WebAPI = WebAPI || {
  TAG: "WebAPI.js",
  APP_TYPE: "WEB",
  BASE_URL: path,
  COMMON_BASE_URL: path,
  LOG_2_CONSOLE: true,
  METHOD_TYPE: {
    POST: "POST",
    GET: "GET"
  },

  //0101.loginAdmin
  loginAdmin: function (account,password, callbackS) {
    var api = "api/admin/login",
      params = {
        account:account,
        password: password
      };

    this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
      callbackS(data);
    });
  },
  //
  getBooklist: function (callbackS) {
      var api = "api/admin/getbooklist",
          params = {
          };
      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
          callbackS(data);
      });
  },

  getMedialist: function (bookid,callbackS) {
      var api = "api/admin/getmedialist",
          params = {
              bookid:bookid
          };
      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
          callbackS(data);
      });
  },

  setBook: function (setting,id,callbackS) {
      var api = "api/admin/setbook/"+id,
          params = {
              setting
          };
      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
          callbackS(data);
      });
  },

  setBookmedia: function (medias,id,callbackS) {
      var api = "api/admin/setbookmedia/"+id,
          params = {
              medias
          };
      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
          callbackS(data);
      });
  },

  dltBook: function (id,callbackS) {
      var api = "api/admin/dltbook/"+id,
          params = {
          };
      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
          callbackS(data);
      });
  },

  resetPassword: function (password, callbackS) {
      var api = "api/admin/resetpassword",
          params = {
              password: password
          };

      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
          callbackS(data);
      });
  },

  getMedia: function (qstr,callbackS) {
      var api = "api/admin/getmedia/"+qstr,
          params = {
          };
      this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
          callbackS(data);
      });
  },


  uploadS3: function (file,filename,dir, callbackS) {
    var api = "api/admin/uploadS3",
      params = {
        filename:filename,
        dir:dir,
        file:file,
      };
    this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
      callbackS(data);
    });
  },








  // auditSaleStatus: function (id,state, callbackS) {
  //   var api = "api/admin/auditsalestatus",
  //     params = {
  //       id:id,
  //       state:state
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // adminSendPush: function (id,msg, callbackS) {
  //   var api = "api/admin/sendpush",
  //     params = {
  //       id:id,
  //       msg:msg
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // setStatus: function (id, callbackS) {
  //   var api = "api/admin/setstatus",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // setOnline: function (id, callbackS) {
  //   var api = "api/admin/setonline",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // //
  //
  // //
  // getSale_wait: function ( callbackS) {
  //   var api = "api/admin/sales_wait",
  //     params = {
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getSale_formal: function ( callbackS) {
  //   var api = "api/admin/sales_formal",
  //     params = {
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getSale_vip: function ( callbackS) {
  //   var api = "api/admin/sales_vip",
  //     params = {
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getTempblacklist: function (callbackS) {
  //   var api = "api/admin/tempblacklist",
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getPhonelistreport: function (id,callbackS) {
  //   var api = "api/admin/phonelistreport/"+id,
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getSetting: function (callbackS) {
  //   var api = "api/admin/setting",
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // setSetting: function (setting,callbackS) {
  //   var api = "api/admin/setsetting",
  //     params = {
  //       setting
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // upgradeApp: function (version,callbackS) {
  //   var api = "api/admin/upgradeVersion",
  //     params = {
  //       version:version
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getMemberlist: function (inpstr, pagesize, pageno, callbackS) {
  //   var api = "api/admin/memberlist",
  //     params = {
  //       inpstr: inpstr,
  //       pagesize: pagesize,
  //       pageno: pageno
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // //
  // getReport_A: function (sd,ed,inpstr, callbackS) {
  //   var api = "api/admin/getreport_A",
  //     params = {
  //       sd:sd,
  //       ed:ed,
  //       inpstr:inpstr
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getReport_B: function (sd,ed,inpstr, callbackS) {
  //   var api = "api/admin/getreport_B",
  //     params = {
  //       sd:sd,
  //       ed:ed,
  //       inpstr:inpstr
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getReport_C: function (sd,ed,inpstr, callbackS) {
  //   var api = "api/admin/getreport_C",
  //     params = {
  //       sd:sd,
  //       ed:ed,
  //       inpstr:inpstr
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // getSaleinfo: function (id, callbackS) {
  //   var api = "api/admin/sales/"+id,
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // saveSaleinfo: function (id,info, callbackS) {
  //   var api = "api/admin/sales/"+id,
  //     params = info;
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // addSaleinfo: function (info, callbackS) {
  //   var api = "api/admin/sales",
  //     params = info;
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // deleteSaleinfo: function (ids, callbackS) {
  //   var api = "api/admin/dltsales",
  //     params = {ids:ids};
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // deletePhonerecord: function (ids, callbackS) {
  //   var api = "api/admin/dltphonerecords",
  //     params = {ids:ids};
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // deleteMembers: function (ids, callbackS) {
  //   var api = "api/admin/dltmembers",
  //     params = {ids:ids};
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getZipcodelist: function ( callbackS) {
  //   var api = "api/front/zipcodelist",
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getPofficelist: function (zip, callbackS) {
  //   var api = "api/front/pofficelist",
  //     params = {
  //       zip:zip,
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // getMemberinfo: function (mid, callbackS) {
  //   var api = "api/admin/member/"+mid,
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  // setMemberinfo: function (mid,obj, callbackS) {
  //   var api = "api/admin/member/"+mid,
  //     params = {
  //       obj
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // getSalereport: function (sd,ed,inpstr,vip, callbackS) {
  //   var api = "api/admin/salesreport",
  //     params = {
  //       sd:sd,
  //       ed:ed,
  //       inpstr:inpstr,
  //       vip:vip
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // getSaleslist: function (callbackS) {
  //   var api = "api/admin/getSaleslist",
  //     params = {
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // getPerformance: function (sd,ed,inpstr,sid,callbackS) {
  //   var api = "api/admin/getPerformance",
  //     params = {
  //       sd:sd,
  //       ed:ed,
  //       inpstr:inpstr,
  //       sid:sid
  //     };
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // getPhonerecord: function ( Startym, Ismember, Salesid, Qrystr, callbackS) {
  //   var api = "api/admin/getPhonerecord",
  //     params = {
  //       Startym: Startym,
  //       Ismember: Ismember,
  //       Salesid: Salesid,
  //       Qrystr: Qrystr
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // saveProductinfo: function (pid,data, callbackS) {
  //   var api = "api/admin/product/"+pid,
  //     params = data;
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // saveProductstate: function (pid,data, callbackS) {
  //   var api = "api/admin/productstate/"+pid,
  //     params = data;
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // addProductinfo: function (data, callbackS) {
  //   var api = "api/admin/product/add",
  //     params = data;
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // replyComment: function (id,reply, callbackS) {
  //   var api = "api/admin/replycomment",
  //     params = {
  //       id:id,
  //       reply:reply
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // setCmtshow: function (id,callbackS) {
  //   var api = "api/admin/setcommentshow",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // setEvalshow: function (id,callbackS) {
  //   var api = "api/admin/setevaluateshow",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // dltCmt: function (id,callbackS) {
  //   var api = "api/admin/dltcomment",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // dltEval: function (id,callbackS) {
  //   var api = "api/admin/dltevaluate",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // //****CR2
  // setAlbumstate: function (id, callbackS) {
  //   var api = "api/admin/setalbumstatus",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // setBlogstatus: function (id, callbackS) {
  //   var api = "api/admin/setblogstatus",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // setReportstatus: function (id, callbackS) {
  //   var api = "api/admin/setreportstatus",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  // dltReport: function (id, callbackS) {
  //   var api = "api/admin/dltreport",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // //**** CR3 ***
  // addMember: function (account,nickname,password,phone,email,state,remark, callbackS) {
  //   var api = "api/admin/member/add",
  //     params = {
  //       account:account,
  //       nickname:nickname,
  //       password:password,
  //       phone:phone,
  //       email:email,
  //       state:state,
  //       remark:remark
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // dltProduct: function (pid, callbackS) {
  //   var api = "api/admin/dltproduct",
  //     params = {
  //       pid:pid
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // dltMember: function (id, callbackS) {
  //   var api = "api/admin/dltmember",
  //     params = {
  //       id:id
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // setRemark: function (id,remark, callbackS) {
  //   var api = "api/admin/setremark",
  //     params = {
  //       id:id,
  //       remark:remark
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // getItemlist: function ( callbackS) {
  //   var api = "api/front/itemlist",
  //     params = {
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.GET, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  // //CR10
  // getIncomelist: function (sdate,edate,state, callbackS) {
  //   var api = "api/admin/incomelist",
  //     params = {
  //       sdate:sdate,
  //       edate:edate,
  //       state:state
  //     };
  //
  //   this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
  //     callbackS(data);
  //   });
  // },
  //
  //


  //All ajax call to server using this method.
  AjaxCall: function (url, obj, methodType, callback, elseopts) {
    var apiCallType, apiName = url.replace(this.BASE_URL, "");
    var isLoadingShow = true;
    if (elseopts){
      //Check the loading type.
      isLoadingShow = elseopts.loadingType != "hide";
    }

    apiCallType = "application/json; charset=utf-8";
    switch (methodType){
      case "GET":
        break;
      case "POST":
        obj = JSON.stringify(obj);
        break;
    }

    // console.log("apiName:", apiName);
    // console.log("url:", url);
    // console.log("obj:", obj);
    $.ajax({
      url: url,
      type: methodType,
      contentType: apiCallType,
      data: obj,
      dataType: "json",
      success: function (data) {
        callback(data);
        // if (data.status != 200){
        //     setTimeout(function () {
        //         mDialog.Check(data.ErrMsg);
        //         localStorage.clear();
        //         // location.href = "login";
        //     }, 1500);
        // }else{
        //     callback(data);
        //     console.log('response: ', data);
        // }
      },
      error: function (data) {
        // console.log("ajax call error: ", data );
        if (data.responseJSON!=undefined && data.responseJSON.message=="請登入"){
          if (mLoading) mLoading.complete();
          mDialog.Check("請重新登入","",function () {
            window.location="login";
          })
        }
        else {
          // mDialog.Check("系統異常，請聯絡工程師");
          callback(data);
        }
      },
      beforeSend: function () {
        switch (apiName){
          case "api/admin/memberexport":
            mLoading.start();
            break;
          case "api/admin/salesexport":
            mLoading.start();
            break;
          case "api/admin/product/add":
            mLoading.start();
            break;
        }
      },
      complete: function () {
        switch (apiName){
          case "api/admin/memberexport":
            mLoading.complete();
            break;
          case "api/admin/salesexport":
            mLoading.complete();
            break;
          case "api/admin/product/add":
            mLoading.complete();
            break;
        }
      }
    })
  }
}