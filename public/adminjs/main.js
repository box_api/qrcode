var dataDB = {
    isTbChange: false,
    tbLength: 20,
    isSearch: false,
    isInfo: false
};

var mHandle = {
    separatedData: {
        numCut: 1000,
        waitTime: 25,
        getData: function (data) {
            var separated = this;
            var objArray = [];  //To put the obj.
            var dataArray = []; //To put the objArray.
            // console.error('separatedData.getData.start(): ', new Date());
            $.each(data, function (i, obj) {
                objArray.push(obj);
                if ((i % separated.numCut == (separated.numCut-1) && i != 0) || ((i+1) == data.length)){
                    dataArray.push(objArray);
                    objArray = [];
                }
            });
            // console.error('separatedData.getData.end(): ', new Date());
            return dataArray;
        },
        getDataRange: function (data, range) {
            var objArray = [];  //To put the obj.
            var dataArray = []; //To put the objArray.
            // console.error('separatedData.getDataRange.start(): ', new Date());
            $.each(data, function (i, obj) {
                objArray.push(obj);
                if ((i % range == (range-1) && i != 0) || ((i+1) == data.length)){
                    dataArray.push(objArray);
                    objArray = [];
                }
            });
            // console.error('separatedData.getDataRange.end(): ', new Date());
            return dataArray;
        },
        setLayout: function (separatedList, $db) {
            var num = 0;
            var handle = setInterval(function () {
                var list = [];
                if (num == 0) {
                    $db.clear().draw();
                    mLoading.start();
                }
                if (num >= separatedList.length){
                    clearInterval(handle);
                    mLoading.complete();
                    return;
                }
                // console.log('Draw DB count:', num);

                $.each(separatedList[num], function (i, item) {
                    if (typeof mMemberInfo != 'undefined' && mMemberInfo != null) list.push(mMemberInfo.getListItem(item));
                    if (typeof mRecord != 'undefined' && mRecord != null) list.push(mRecord.getListItem(item));
                    if (typeof mMember != 'undefined' && mMember != null) list.push(mMember.getListItem(item));
                });

                $db
                    .rows
                    .add(list)
                    .draw();
                num++;
            }, this.waitTime);
        }
    },
    split: function (data, index) {
        var val = '';
        if (data.indexOf('&') > 0){
            var dataArray = data.split('&');
            val = dataArray[index].split('=')[1];
        } else{
            val = data.split('=')[1];
        }
        return val;
    },
    progress: {
        dataLength: 0,
        num: 0,
        count: 0,
        avgNum: 0,
        handle: '',
        setLayout: function (progressNum) {
            var loadNum = mLoading.uploadNumID;
            $('#'+loadNum).text(progressNum);
        },
        addCount: function () {
            if (this.count < this.dataLength) {
                this.count++;
                if (this.count >= this.dataLength) this.num = 100;
                else this.num = Math.floor(this.avgNum * this.count);
            }
        },
        run: function () {
            var progress = this;
            this.handle = setInterval(function () {
                progress.setLayout(progress.num);
                if (progress.num >= 100){
                    clearInterval(progress.handle);
                    return;
                }
                // console.log('progress.num: ', progress.num);
                // console.log('progress.avgNum * (progress.count+1): ', progress.avgNum * (progress.count+1));
                if (progress.num < Math.floor(progress.avgNum * (progress.count+1))) progress.num++;  //If auto add num be large then api callback count num, stop to add.
            }, 500);
        },
        init: function (length) {
            this.dataLength = length;
            this.count = 0;
            this.num = 0;
            this.avgNum = 100 / length;
            this.run();
        }
    },
    getThisLastDate: function () {
        var date = new Date();
        var thisM = date.getMonth() + 1;
        var lastM = date.getMonth();
        var thisY = date.getFullYear();
        var lastY = date.getFullYear() - 1;
        // console.log("thisM: ",thisM);
        // console.log("lastM: ",lastM);
        // console.log("thisY: ",thisY);
        // console.log("lastY: ",lastY);
    }
};

// var testJSON = {
//     total: 987,
//     pageCount: 50,
//     getData: function () {
//         var data = [];
//         var str1 = ['你', '她', '我', '我們', '他們'];
//         var str2 = ['是', '喜歡', '有', '想要', '的'];
//         var str3 = ['食物', 'Su', '臭雞', '運動', '睡覺'];
//
//
//         for (var i=0; i < this.total; i++){
//             var dateY = Math.floor((Math.random() * 10) + 2010), dateM = Math.floor((Math.random() * 12) + 1);
//             var str = str1[Math.floor(Math.random() * 4)] + str2[Math.floor(Math.random() * 4)] + str3[Math.floor(Math.random() * 4)];
//             data.push({
//                 Datestr: dateY + '.' + dateM,
//                 Salesname: "業務" + i,
//                 Remark: str
//             });
//         }
//         return data;
//     }
// };
//
// var mUser = {
//     Token: sessionStorage['token'] ? sessionStorage['token'] : ''
// };
// console.log(mUser.Token);

var mDialog = {
    bufferTime: 500,
    systemTitle: '系統提示',
    cancelBtnName: '取消',
    checkBtnName: '確定',
    defaultLayout: function (eln) {
        //Middle the dialog in the window.
        $(eln).attr('style','display: flex;');
    },
    defaultSystem: function (eln) {
        /*Default the title, button name*/
        $(eln).find('.modal-title').text(this.systemTitle);
        if ($(eln).is('#sConfirm-dialog')){
            $(eln).find('.modal-footer button:last-child').text(this.checkBtnName);
            $(eln).find('.modal-footer button:first-child').text(this.cancelBtnName);
        }else if($(eln).is('#sCheck-dialog')){
            $(eln).find('.modal-footer button:first-child').text(this.checkBtnName);
        }
    },
    preventScroll: function () {
        //Prevent the body's padding-right and scroll trigger more than once.
        var isNoDialogIn = true;
        $('.dialog-main').each(function (i, dialog) {
            if ($(dialog).hasClass('in')){
                setTimeout(function () {
                    $('body').addClass('modal-open').attr('style', 'padding-right: 17px;');
                }, 400);
                isNoDialogIn = false;
                return false;
            }
        });
        if (isNoDialogIn){
            setTimeout(function () {
                var $body = $('body');
                if ($body.hasClass('modal-open')){
                    $body.removeClass('modal-open');
                }
                if ($body.attr('style') != null && $body.attr('style') != ''){
                    $body.removeAttr('style');
                }
                // console.log('preventScroll');
            }, 400);
        }
    },
    Confirm: function (content, title, callback, btnOpts, event) {
        var mdialog = this;
        setTimeout(function () {
            $('#sConfirm-dialog').on('show.bs.modal', function () {
                var eln = this;
                mdialog.defaultLayout(eln);
                mdialog.defaultSystem(eln);

                if (title) $(eln).find('.modal-title').html(title);
                $(eln).find('.dialog-body').html(content);
                if (btnOpts){
                    if (btnOpts.checkName) $(eln).find('.modal-footer button:last-child').text(btnOpts.checkName);
                    if (btnOpts.cancelName) $(eln).find('.modal-footer button:first-child').text(btnOpts.cancelName);
                }
                if (event) event();

                $(eln).off().on('click', function (e) {
                    if ($(e.target).is('#sConfirm-dialog') || $(e.target).is('#confirmCancel')){
                        $(eln).modal('hide');
                        mdialog.preventScroll();
                    }else if ($(e.target).is('#confirmBtn')){
                        if (callback){
                            callback({
                                close: function () {
                                    $(eln).modal('hide');
                                    mdialog.preventScroll();
                                },
                                eln: eln
                            });
                        }else{
                            $(eln).modal('hide');
                            mdialog.preventScroll();
                        }
                    }
                });
            }).modal('show');
        }, mdialog.bufferTime);
    },
    Check: function (content, title, callback, btnOpts, event) {
        var mdialog = this;
        setTimeout(function () {
            $('#sCheck-dialog').on('show.bs.modal', function () {
                var eln = this;
                mdialog.defaultLayout(eln);
                mdialog.defaultSystem(eln);

                if (title) $(eln).find('.modal-title').html(title);
                $(eln).find('.dialog-body').html(content);
                if (btnOpts) if (btnOpts.checkName) $(eln).find('.modal-footer button:first-child').text(btnOpts.checkName);
                if (event) event();

                $(eln).off().on('click', function (e) {
                    if ($(e.target).is('#sCheck-dialog') || $(e.target).is('#checkBtn')){
                        if (callback){
                            callback({
                                close: function () {
                                    $(eln).modal('hide');
                                    mdialog.preventScroll();
                                },
                                eln: eln
                            });
                        }else{
                            $(eln).modal('hide');
                            mdialog.preventScroll();
                        }
                    }
                });
            }).modal('show');
        }, mdialog.bufferTime);
    }
};

var mLoading = {
    eln: '<div id="loading" class="loading-bg"><div class="loading-body"><div class="loading-logo"></div><div class="loading-txt"></div></div></div>',
    loadCount: 0,
    isLoading: false,
    customEln: '',
    uploadNumID: '',
    start: function (role) {
        var $body = $('body');
        if (this.isLoading){
            //Record the how many loading be called.
            this.loadCount = this.loadCount++;
        }else{
            var mEln = '';
            switch (role) {
                case 'loading':
                case 'uploading':
                    mEln = this.customEln;
                    break;
                default:
                    mEln = this.eln;
                    break;
            }
            $body.append($(mEln).fadeIn(500).removeAttr('style')).addClass('scrollHide');
            this.isLoading = true;
        }
    },
    complete: function () {
        // console.log("this.loadCount: ",this.loadCount);
        var $body = $('body');
        if (this.loadCount == 0 && this.isLoading ){
            $body.removeClass('scrollHide').removeAttr('style');
            $('#loading').remove();
            // console.error('loading complete');
            this.isLoading = false;
            return true;
        }else if (this.loadCount > 0){
            this.isLoading = this.isLoading--;
            return false;
        }
    },
    setCustomAlert: function (alert, type) {
        var _laod = this;
        var alertEln = '', idName = '';
        switch (type) {
            case 'txt-loading':
                var strs = alert.split('');
                $.each(strs, function (i, t) {
                    alertEln += '<i class="txt-loading">'+t+'</i>';
                });
                break;
            case 'uploading':
                var arr = alert.split('#');
                $.each(arr, function (i, t) {
                    if (i != 1){
                        var s = t.split('');
                        $.each(s, function (j, k) {
                            alertEln += '<i class="txt-uploading">'+k+'</i>';
                        });
                    }else{
                        alertEln += '<i id="uploadNum" class="txt-uploading">'+t+'</i>';
                        idName = 'uploadNum';
                        _laod.uploadNumID = idName;
                    }
                });
                break;
        }
        this.customEln = '<div id="loading" class="loading-bg"><div class="loading-body"><div class="loading-logo"></div><div class="loading-txt">'+alertEln+'</div></div></div>';
        return idName;
    }
};

$(function () {
    var pageName = window.location.pathname;
    // if (!mUser.Token && pageName != "/login"){
    //     // window.location.href = "login";
    // }

    var tmp=  $(".side-menu").find("li");
    if (pageName.indexOf("phonelist")>-1){
        $(tmp[1]).addClass("current-page")
    }
    else if (pageName.indexOf("membercontent")>-1){
      $(tmp[0]).addClass("current-page")
    }
    else if (pageName.indexOf("videocontent")>-1){
        $(tmp[1]).addClass("current-page")
    }

    var $radioBtn = $('.radioBtn');
    if ($radioBtn.length > 0){
        $radioBtn.click(function () {
            var isChecked = $(this).prev('input[type="radio"]').is(':checked');
            var $thisBlock = $(this).parents('div[role="select"]');
            if (isChecked){
                $(this).prev('input[type="radio"]').prop('checked', false);
                //Not allow the input or select be used.
                if ($thisBlock.find('input[type="text"]').length > 0) $thisBlock.find('input[type="text"]').prop('disabled', true);
                if ($thisBlock.find('select').length > 0) $thisBlock.find('select').prop('disabled', true);
                if ($('.buttons-excel').length > 0) $('#exportExcel').addClass('hidden');
                return false;
            }else{
                // $radioBtn.each(function (i, btn) {
                //     var isElseChecked = $(btn).prev('input[type="radio"]').is(':checked');
                //     if (isElseChecked){
                //         var $block = $(btn).parents('div[role="select"]');
                //         $(btn).prev('input[type="radio"]').prop('checked', false);
                //         //Not allow the input or select be used.
                //         if ($block.find('input[type="text"]').length > 0) $block.find('input[type="text"]').prop('disabled', true);
                //         if ($block.find('select').length > 0) $block.find('select').prop('disabled', true);
                //         return false;
                //     }
                // });
                //Allow the input or select be used.
                if ($thisBlock.find('input[type="text"]').length > 0) $thisBlock.find('input[type="text"]').prop('disabled', false);
                if ($thisBlock.find('select').length > 0) $thisBlock.find('select').prop('disabled', false);
                if ($('.buttons-excel').length > 0 && $('#searchType').length > 0 && $('#searchType').val() == 'N') $('#exportExcel').removeClass('hidden');
            }
        });
    }

    // var $inputDates = $('.input-date-block');
    // if ($inputDates.length > 0){
    //     $inputDates.datepicker({
    //         inputs: $('.input-date'),
    //         format: 'yyyy/mm/dd',
    //         maxViewMode: 0
    //     });
    //
    //     // $inputDates.find('input').each(function () {
    //     //     $(this).datepicker('clearDates');
    //     // });
    //
    // }

    var $checkAll = $('#checkAll');
    if ($checkAll.length > 0){
        $checkAll.change(function () {
            if ($checkAll.is(':checked'))
                $('input[name="selectBox"]').prop('checked', true);
            else
                $('input[name="selectBox"]').prop('checked', false);
        });
    }
});

function getFile(file) {
  var reader = new FileReader();
  return new Promise((resolve, reject) => {
    reader.onerror = () => { reader.abort(); reject(new Error("Error parsing file"));}
    reader.onload = function () {

      //This will result in an array that will be recognized by C#.NET WebApi as a byte[]
      let bytes = Array.from(new Uint8Array(this.result));

      //if you want the base64encoded file you would use the below line:
      let base64StringFile = btoa(bytes.map((item) => String.fromCharCode(item)).join(""));

      //Resolve the promise with your custom file structure
      resolve({
        bytes: bytes,
        base64StringFile: base64StringFile,
        fileName: file.name,
        fileType: file.type
      });
    }
    reader.readAsArrayBuffer(file);
  });
}