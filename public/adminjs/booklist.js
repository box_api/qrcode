/**
 * Created by ralph on 3/14/2020.
 */
// var tmprec={"code":0,"data":[
//     {"createdAt":"2020-02-01","title":"英語一","code":"eng-01","brief":"英語一簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-02","title":"英語二","code":"eng-02","brief":"英語二簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-03","title":"英語三","code":"eng-03","brief":"英語三簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-04","title":"英語四","code":"eng-04","brief":"英語四簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-05","title":"英語五","code":"eng-05","brief":"英語五簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-06","title":"英語六","code":"eng-06","brief":"英語六簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-07","title":"英語七","code":"eng-07","brief":"英語七簡介","btn":btn,"btn1":btnexp},
//     {"createdAt":"2020-02-08","title":"英語八","code":"eng-08","brief":"英語八簡介","btn":btn,"btn1":btnexp}]};

var mBook = {
    searchTxt: '',
    pageNo: 0,
    getList: function (callback) {
        // callback(tmprec)
        WebAPI.getBooklist( function (data) {
            callback(data);
        });
    },
    getListItem: function (data) {
        var btn='<button type="button" class="btn btn-warning " rel="'+data.id+'" onclick="linkContent('+data.id+')">編輯影音檔</button>';
        var btnexp='<button type="button" class="btn btn-primary downzip" onclick="downzip('+data.id+')">匯出qrcode</button>';
        return [moment(data.createdAt).format("YYYY-MM-DD"),data.title,data.code,data.brief,btn,btnexp];
    },
    init: function ($db) {
        // var sd=$("#searchDateS").val()==""||$("#searchDateS").is(':disabled')?"":moment($("#searchDateS").val()).format("YYYY-MM-DD 00:00:00");
        // var ed=$("#searchDateE").val()==""||$("#searchDateE").is(':disabled')?"":moment($("#searchDateE").val()).format("YYYY-MM-DD 23:59:59");
        // var vip=($("#d-radioB").prop("checked"))?"Y":"N";
        // console.log($("#searchDateS").is(':disabled'))
        // return;
        mLoading.start();
        this.getList(function (data) {
            if (data.code=="0"){
                var list = [];
                var dataList = data.data;
                if (dataList.length > 0) {
                    $.each(dataList, function (i, item) {
                        list.push(mBook.getListItem(item));
                    });
                }
                //
                $("#h5Cnt").html("資料筆數:"+data.data.length)
                // $db.pageCount=data.cnt;
                $db
                    .clear()
                    .rows
                    .add(list)
                    .draw();
                mLoading.complete();
            }
            else{
                mLoading.complete();
                mDialog.Check(data.responseJSON.message);
            }
        });
    }
}

function downzip(id) {
    window.location.href="api/admin/getzip/"+id;
}
function linkContent(id) {
    window.location.href="videocontent?id="+id;
}
$(function () {

    // $("#searchDateS").datepicker({
    //     format: "yyyy-mm-dd",
    //     startView: "month",
    //     minViewMode: "days",
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     $(this).datepicker('hide');
    // });
    // $("#searchDateE").datepicker({
    //     format: "yyyy-mm-dd",
    //     startView: "month",
    //     minViewMode: "days",
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     $(this).datepicker('hide');
    // });
    //
    //
    var $db = $('#dbBook').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        order: [[0, 'desc']],
        // columnDefs: [{
        //     targets: [0,1,2,3,4],
        //     orderable: false
        // }],
        searching: true,
        stateSave: true,
        language: {
            search: "<label style='font-size: 14px;font-weight: bold;color: black'>關鍵字查詢</label>:"
        },
        info: dataDB.isInfo
    });

    mBook.init($db);

    $('#dbBook').on('click', 'tr', function(event) {
        var tr=$(event.target).closest("tr");
        var th=$(event.target).closest("th");
        if (event.target.nodeName=="BUTTON"||th.length>0){
            // if($(event.target).html()=="編輯影音檔"){
            //     window.location.href="videocontent";
            // }
        }
        else {
            showAddModal("E",tr);
        }
    })

    $("#btnSearch").on("click", function () {
        mBook.init($db);
    })

    $("#btnAdd").on("click", function () {
        showAddModal("A");
    })

    function showAddModal(type,tr) {
        var txt=(type=="A")?"新增書本":"編輯書本";
        var title="",code="",brief="",id=0;
        if (type=="E"){
            title=$(tr[0].cells[1]).html();
            code=$(tr[0].cells[2]).html();
            brief=$(tr[0].cells[3]).html();
            id=$($(tr[0].cells[4]).find("button")[0]).attr("rel");
        }
        $("#h4Modal").html(txt);
        $("#txtName").val(title);$("#txtCode").val(code);$("#txtBrief").val(brief);
        if(type=="A"){
            $("#btnDelete").hide();
        }
        else{
            $("#btnDelete").show();
        }
        $("#addModal").on('show.bs.modal', function () {
            $("#btnSave").unbind().bind("click", function () {
                if ($("#txtName").val()==""){
                    mDialog.Check("請輸入書名");
                }
                else if ($("#txtCode").val()==""){
                    mDialog.Check("請輸入代碼");
                }
                // else if ($("#txtBrief").val()==""){
                //     mDialog.Check("請輸入簡述");
                // }
                else {
                    title=$("#txtName").val();code=$("#txtCode").val();brief=$("#txtBrief").val();
                    WebAPI.setBook({title:title,code:code,brief:brief},id,function (data) {
                        if (data.code=="0") {
                            $("#addModal .close").click();
                            mDialog.Check("儲存作業成功");
                            mBook.init($db);
                        }
                        else{
                            mDialog.Check("儲存設定作業失敗:"+data.responseJSON.message);
                        }
                    })
                }
            })
            $("#btnDelete").unbind().bind("click",function () {
                mDialog.Confirm("你確定要刪除此書本? 刪除後影音資料將一併刪除，資料將無法回復","請確認",function (ok) {
                    if (ok){
                        WebAPI.dltBook(id,function (data) {
                            if (data.code=="0"){
                                mDialog.Check("刪除作業成功","",function (res) {
                                    window.location.href="booklist";
                                })
                            }
                            else {
                                mDialog.Check("作業發生錯誤:"+data.responseJSON.message)
                            }
                        })
                    }
                })
            })
        }).modal('show');
    }

})