var btnedit='<button type="button" class="btn btn-warning edit">編輯</button>';
var btndlt='<button type="button" class="btn btn-danger " >刪除</button>';


var tmprec=
    {"code":0,"data":[
        {"pagenum":"11","filename":"英語1-11.mp3","type":"家長","brief":"英語影音一-11簡介","createdAt":"2020-02-01"},
        {"pagenum":"12","filename":"英語1-12.mp3","type":"老師","brief":"英語影音一-12簡介","createdAt":"2020-02-02"},
        {"pagenum":"13","filename":"英語1-13.mp3","type":"家長","brief":"英語影音一-13簡介","createdAt":"2020-02-03"},
        {"pagenum":"14","filename":"英語1-14.mp3","type":"老師","brief":"英語影音一-14簡介","createdAt":"2020-02-04"},
        {"pagenum":"15","filename":"英語1-15.mp3","type":"家長","brief":"英語影音一-15簡介","createdAt":"2020-02-05"},
        {"pagenum":"16","filename":"英語1-16.mp3","type":"老師","brief":"英語影音一-16簡介","createdAt":"2020-02-06"},
        {"pagenum":"17","filename":"英語1-17.mp3","type":"老師","brief":"英語影音一-17簡介","createdAt":"2020-02-07"},
        {"pagenum":"18","filename":"英語1-18.mp3","type":"老師","brief":"英語影音一-18簡介","createdAt":"2020-02-08"},
        ]};

var mBook = {
    searchTxt: '',
    pn: 0,
    getList: function (sd,ed,inpstr,vip,callback) {
        callback(tmprec)
        // WebAPI.getSalereport(sd,ed,inpstr,vip, function (data) {
        //     callback(data);
        // });
    },
    getListItem: function (data) {
        //頁碼	檔名	適用對象	簡述	建立日期	編輯	刪除
        return [data.pagenum,data.filename,data.type,data.brief,data.createdAt,btnedit,btndlt];
    },
    init: function ($db) {
        var sd=$("#searchDateS").val()==""||$("#searchDateS").is(':disabled')?"":moment($("#searchDateS").val()).format("YYYY-MM-DD 00:00:00");
        var ed=$("#searchDateE").val()==""||$("#searchDateE").is(':disabled')?"":moment($("#searchDateE").val()).format("YYYY-MM-DD 23:59:59");
        var vip=($("#d-radioB").prop("checked"))?"Y":"N";
        // console.log($("#searchDateS").is(':disabled'))
        // return;
        mLoading.start();
        this.getList(sd,ed,$("#searchInput").val(),vip, function (data) {
            if (data.code=="0"){
                var list = [];
                var dataList = data.data;
                if (dataList.length > 0) {
                    $.each(dataList, function (i, item) {
                        list.push(mBook.getListItem(item));
                    });
                }
                //
                $("#h5Cnt").html("資料筆數:"+data.data.length)
                // $db.pageCount=data.cnt;
                $db
                    .clear()
                    .rows
                    .add(list)
                    .draw();
                mLoading.complete();
            }
            else{
                mLoading.complete();
                mDialog.Check(data.responseJSON.message);
            }
        });
    }
}

$(function () {

    // $("#searchDateS").datepicker({
    //     format: "yyyy-mm-dd",
    //     startView: "month",
    //     minViewMode: "days",
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     $(this).datepicker('hide');
    // });
    // $("#searchDateE").datepicker({
    //     format: "yyyy-mm-dd",
    //     startView: "month",
    //     minViewMode: "days",
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     $(this).datepicker('hide');
    // });
    //
    //
    var $db = $('#dbVideo').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        order: [[0, 'desc']],
        // columnDefs: [{
        //     targets: [0,1,2,3,4],
        //     orderable: false
        // }],
        searching: true,
        stateSave: true,
        language: {
            search: "<label style='font-size: 14px;font-weight: bold;color: black'>關鍵字查詢</label>:"
        },
        info: dataDB.isInfo
    });

    mBook.init($db);

    $('#dbVideo').on('click', 'tr', function(event) {
        // var cb=$(this);
        window.location.href="videocontent";
    })

    // $("#btnSearch").on("click", function () {
    //     mBook.init($db);
    // })

    $("#btnAdd").on("click", function () {
        window.location.href="videocontent";
        // $("#addModal").on('show.bs.modal', function () {
        //     $("#btnSave").unbind().bind("click", function () {
        //         if ($("#txtSetting").val()==""){
        //             mDialog.Check("請輸入分數");
        //         }
        //         else {
        //             $("#addModal .close").click();
        //             // WebAPI.setSetting({Star:$("#txtSetting").val()},function (data) {
        //             //     if (data.code=="0") {
        //             //         $("#settingModal .close").click();
        //             //         mDialog.Check("儲存作業成功");
        //             //         bMember.init($dbB,true);
        //             //     }
        //             //     else{
        //             //         mDialog.Check("儲存設定作業失敗");
        //             //     }
        //             // })
        //         }
        //     })
        // }).modal('show');
    })

    // function showAddModal(type) {
    //     var txt=(type=="A")?"新增書本":"編輯書本";
    //     $("#h4Modal").html(txt);
    //     if(type=="A"){
    //         $("#btnDelete").hide();
    //     }
    //     else{
    //         $("#btnDelete").show();
    //     }
    //     $("#addModal").on('show.bs.modal', function () {
    //         $("#btnSave").unbind().bind("click", function () {
    //             if ($("#txtSetting").val()==""){
    //                 mDialog.Check("請輸入分數");
    //             }
    //             else {
    //                 $("#addModal .close").click();
    //                 // WebAPI.setSetting({Star:$("#txtSetting").val()},function (data) {
    //                 //     if (data.code=="0") {
    //                 //         $("#settingModal .close").click();
    //                 //         mDialog.Check("儲存作業成功");
    //                 //         bMember.init($dbB,true);
    //                 //     }
    //                 //     else{
    //                 //         mDialog.Check("儲存設定作業失敗");
    //                 //     }
    //                 // })
    //             }
    //         })
    //     }).modal('show');
    // }

})