var vfmt=["mp4","mpeg","wmv","m4v","avi","ogm","webm","asx","mov"];
var afmt=["mp3","m4a","wma","flac","ogg","acc","mpeg","wav"];
function gettaghtml(url) {
    var tmp=url.split('.');
    var ext=tmp[tmp.length-1];
    if (vfmt.indexOf(ext)>-1)
        return getvideo(url,ext);
    else if (afmt.indexOf(ext)>-1)
        return getaudio(url,ext);
    else
        return getimage(url);
}
function getvideo(url,ext) {
    var res='<video id="mediaPreview" playsinline autoplay controls style="width: 100%;" src="'+url+'" type="video/'+ext+'">'+
        // '<source id="mediaSource" src="'+url+'" type="video/'+ext+'">'+
        '</video>';
    return res;
}
function getaudio(url,ext) {
    var res='<audio id="mediaPreview" playsinline autoplay controls style="width: 100%;" src="'+url+'" type="video/'+ext+'">'+
        // '<source id="mediaSource" src="'+url+'" type="audio/'+ext+'">'+
        '</audio>';
    return res;
}
function getimage(url) {
    var res='<img id="mediaPreview" src="'+url+'"  style="width: 100%;">';
        // '<source id="mediaSource" src="'+url+'" >'+
        // '</img>';
    return res;
}
$(function () {
    // console.log(window.location.search)

    WebAPI.getMedia(window.location.search.substr(1),function (data) {
        if (data.code==0){
            // console.log(data.data)
            if (data.data.length>0){
                var md=data.data[0];
                $("h3.title-page").html(md.title);
                $("h4.title-page").html(md.brief);
                $("#divMedia").html(gettaghtml(md.url));
                // $("#mediaPreview").get(0).pause();
                // $("#mediaSource").attr("src",md.url);
                // $("#mediaPreview").get(0).load();
                // $("#mediaPreview").get(0).play();
            }
            else{
                mDialog.Check("無法取得影音資料");
            }
        }
        else{
            mDialog.Check("取得影音資料發生錯誤:"+data.responseJSON.message)
        }
    })


    $("#btnScan").click(function () {
        window.location.href="qrscan";
    })
})