$(function () {

  $("#btnCfm").click(function () {
    var p1=$("#txtPwd").val();
    var p2=$("#txtCfmPwd").val();
    if (p1==""||p2==""){
      mDialog.Check("新密碼及確認密碼不可空白");
    }
    else if (p1!=p2){
      mDialog.Check("新密碼及確認密碼不一致");
    }
    else{
      WebAPI.resetPassword(p1,function (data) {
        if (data.status==200){
          mDialog.Check("更新密碼成功!");
        }
        else{
          mDialog.Check("更新密碼失敗:"+data.responseText);
        }
      })

    }
  })
})