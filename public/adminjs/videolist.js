/**
 * Created by ralph on 3/14/2020.
 */
var tmprec={"code":0,"data":[{"createdAt":"2020-02-01","title":"英語一","pagenum":"11","position":"第一行","type":"家長","brief":"英語影音一簡介"},
    {"createdAt":"2020-02-02","title":"英語2","pagenum":"12","position":"第2行","type":"老師","brief":"英語影音2簡介"},
    {"createdAt":"2020-02-03","title":"英語3","pagenum":"13","position":"第3行","type":"老師","brief":"英語影音3簡介"},
    {"createdAt":"2020-02-04","title":"英語4","pagenum":"14","position":"第4行","type":"老師","brief":"英語影音4簡介"},
    {"createdAt":"2020-02-05","title":"英語5","pagenum":"15","position":"第5行","type":"家長","brief":"英語影音5簡介"},
    {"createdAt":"2020-02-06","title":"英語6","pagenum":"16","position":"上","type":"家長","brief":"英語影音6簡介"},
    {"createdAt":"2020-02-07","title":"英語7","pagenum":"17","position":"中","type":"老師","brief":"英語影音7簡介"},
    {"createdAt":"2020-02-08","title":"英語8","pagenum":"18","position":"下","type":"家長","brief":"英語影音8簡介"}]};

var mBook = {
    searchTxt: '',
    pn: 0,
    getList: function (id,callback) {
        // callback(tmprec)
        WebAPI.getMedialist(id, function (data) {
            callback(data);
        });
    },
    getListItem: function (data) {
        var lbl='<label rel="'+data.bookid+'">'+moment(data.createdAt).format("YYYY-MM-DD")+'</label>';
        // var typtxt=(data.type=="T")?'<span style="color:blue">老師</span>': "家長";
        return [lbl,data.title,data.pagenum,data.filename,data.type,data.brief];
    },
    init: function ($db) {
        // var sd=$("#searchDateS").val()==""||$("#searchDateS").is(':disabled')?"":moment($("#searchDateS").val()).format("YYYY-MM-DD 00:00:00");
        // var ed=$("#searchDateE").val()==""||$("#searchDateE").is(':disabled')?"":moment($("#searchDateE").val()).format("YYYY-MM-DD 23:59:59");
        // var vip=($("#d-radioB").prop("checked"))?"Y":"N";
        // console.log($("#searchDateS").is(':disabled'))
        // return;
        mLoading.start();
        this.getList($("#searchType").val(), function (data) {
            if (data.code=="0"){
                var list = [];
                var dataList = data.data;
                if (dataList.length > 0) {
                    $.each(dataList, function (i, item) {
                        list.push(mBook.getListItem(item));
                    });
                }
                //
                $("#h5Cnt").html("資料筆數:"+data.data.length)
                // $db.pageCount=data.cnt;
                $db
                    .clear()
                    .rows
                    .add(list)
                    .draw();
                mLoading.complete();
            }
            else{
                mLoading.complete();
                mDialog.Check(data.responseJSON.message);
            }
        });
    }
}

function setSelecttype() {
    $("#searchType").find('option').not(':first').remove();
    $("#searchType").append('<option value="" selected>全部</option>')
    WebAPI.getBooklist( function (data) {
        if (data.code==0){
            data.data.forEach(function (val) {
                $("#searchType").append('<option value="'+val.id+'">'+val.title+'</option>')
            })

        }
    });
}
$(function () {
    setSelecttype();
    // $("#searchDateS").datepicker({
    //     format: "yyyy-mm-dd",
    //     startView: "month",
    //     minViewMode: "days",
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     $(this).datepicker('hide');
    // });
    // $("#searchDateE").datepicker({
    //     format: "yyyy-mm-dd",
    //     startView: "month",
    //     minViewMode: "days",
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     $(this).datepicker('hide');
    // });
    //
    //
    var $db = $('#dbVideo').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        order: [[0, 'desc']],
        // columnDefs: [{
        //     targets: [0,1,2,3,4],
        //     orderable: false
        // }],
        searching: true,
        stateSave: true,
        language: {
            search: "<label style='font-size: 14px;font-weight: bold;color: black'>關鍵字查詢</label>:"
        },
        info: dataDB.isInfo
    });

    mBook.init($db);

    $('#dbVideo').on('click', 'tr', function(event) {
        var tr=$(event.target).closest("tr");
        var th=$(event.target).closest("th");
        if (th.length>0){

        }
        else if ($(tr[0].cells[0]).find("label").length>0){
            var id=$($(tr[0].cells[0]).find("label")[0]).attr("rel");
            window.location.href="videocontent?id="+id;
        }
        else{
            mDialog.Check("無法取得書本id");
        }
    })

    $("#btnSearch").on("click", function () {
        mBook.init($db);
    })

    $("#btnAdd").on("click", function () {
        window.location.href="videocontent";
        // $("#addModal").on('show.bs.modal', function () {
        //     $("#btnSave").unbind().bind("click", function () {
        //         if ($("#txtSetting").val()==""){
        //             mDialog.Check("請輸入分數");
        //         }
        //         else {
        //             $("#addModal .close").click();
        //             // WebAPI.setSetting({Star:$("#txtSetting").val()},function (data) {
        //             //     if (data.code=="0") {
        //             //         $("#settingModal .close").click();
        //             //         mDialog.Check("儲存作業成功");
        //             //         bMember.init($dbB,true);
        //             //     }
        //             //     else{
        //             //         mDialog.Check("儲存設定作業失敗");
        //             //     }
        //             // })
        //         }
        //     })
        // }).modal('show');
    })

    // function showAddModal(type) {
    //     var txt=(type=="A")?"新增書本":"編輯書本";
    //     $("#h4Modal").html(txt);
    //     if(type=="A"){
    //         $("#btnDelete").hide();
    //     }
    //     else{
    //         $("#btnDelete").show();
    //     }
    //     $("#addModal").on('show.bs.modal', function () {
    //         $("#btnSave").unbind().bind("click", function () {
    //             if ($("#txtSetting").val()==""){
    //                 mDialog.Check("請輸入分數");
    //             }
    //             else {
    //                 $("#addModal .close").click();
    //                 // WebAPI.setSetting({Star:$("#txtSetting").val()},function (data) {
    //                 //     if (data.code=="0") {
    //                 //         $("#settingModal .close").click();
    //                 //         mDialog.Check("儲存作業成功");
    //                 //         bMember.init($dbB,true);
    //                 //     }
    //                 //     else{
    //                 //         mDialog.Check("儲存設定作業失敗");
    //                 //     }
    //                 // })
    //             }
    //         })
    //     }).modal('show');
    // }

})