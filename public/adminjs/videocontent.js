
function setSelecttype(id) {
    $("#sltBook").find('option').remove();
    $("#sltBook").append('<option value="">請選擇書本</option>');
    WebAPI.getBooklist( function (data) {
        if (data.code==0){
            data.data.forEach(function (val) {
                $("#sltBook").append('<option value="'+val.id+'">'+val.title+'</option>')
            })
            if (id!=null && id!=""){
                $("#sltBook").val(id);
                $("#sltBook").prop('disabled',true);
            }
            else {
                $("#sltBook").prop('disabled',false);
            }
        }
    });
}
var Aws = {
    progressNum: 0,
    guid: function (_string) {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return ((s4() + s4() + '-' + s4() /*+ '-' + s4() + '-' + s4() + '-' + s4() + s4() +s()*/) + _string).toString();
    },

    uploadFileAWS:async function(file, callback) {
        if (file) {

            var typeName = file.type.split('/')[0];
            // console.log(typeName);
            var dir=(typeName=="video")?'video':'audio';
            var fileEx = file.type.split('/')[1];
            var sFileName = this.guid(file.size);
            var sFileFullName = sFileName + '.' + fileEx;
              var sndfile=await getFile($("#fUpload").prop('files')[0]);
              WebAPI.uploadS3(sndfile,sFileFullName,dir,function (res) {
                if (res.code=="0"){
                  setTimeout(function () {
                    callback('https://mediaport.s3.amazonaws.com/'+dir+'/'+sFileFullName,file);
                  },1000)
                }
              })

        } else {
            mDialog.Check('Nothing to upload.') ;
        }
    },

}

function getSeq(jsonData,pagenum) {
    var tmp=jsonData.filter(function (val) {
        return parseInt(val.pagenum)==parseInt(pagenum);
    })
    return tmp.length+1;
}
function appendJson(data) {
    var jsonData=JSON.parse(localStorage["jsonData"]);
    jsonData.push(data);
    saveJson(jsonData);
}
function saveJson(data) {
    localStorage["jsonData"]=JSON.stringify(data);
}
function sortJson(fld) {
    var jsonData=JSON.parse(localStorage["jsonData"]);
    jsonData.sort(function(a, b) {

        return a[fld] < b[fld] ? -1 : a[fld] > b[fld] ? 1 : 0;
    });
    return jsonData;
}

function getMxid() {
    var arr=JSON.parse(localStorage["jsonData"]);
    if (arr.length==0) return 1;
    var max;
    for (var i=0 ; i<arr.length ; i++) {
        if (max == null || parseInt(arr[i]["id"]) > parseInt(max["id"]))
            max = arr[i];
    }
    return parseInt(max["id"])+1;
}

$("#fUpload").change(function(event){
    var len=0,cnt=0;
    if ($("#fUpload")[0].files.length>0){
        mLoading.start();
        for (var i=0;i<$("#fUpload")[0].files.length;++i){
            len= $("#fUpload")[0].files.length;
            Aws.uploadFileAWS($("#fUpload")[0].files[i],function (res,file) {
                if (res!="error"){
                    var mxid=getMxid();
                    var imghtml=pimgHtml({"filename":file.name,"url":res,"pagenum":"","seq":"","brief":"","id":mxid});
                    $(imghtml).insertBefore($("#pUpload"));
                    appendJson({"filename":file.name,"url":res,"pagenum":"","seq":"","brief":"","id":mxid});
                    cbComplete();
                }
            })
        }
    }
    else {
        mDialog.Check("請選取圖片")
    }
    function cbComplete() {
        ++cnt;
        if (len==cnt)
            mLoading.complete();
    }
});

//single upload switch src url only
function singleUpload(id) {
    $("#sUpload").attr("rel",id)
    $("#sUpload").trigger("click");
}
$("#sUpload").change(function(event){
    event.stopPropagation()
    var sid=$("#sUpload").attr("rel")
    if ($("#sUpload")[0].files.length>0){
        var tmp=$(".pimg");
        var slt=tmp.filter(function (idx,val) {
            // console.log(val)
            return parseInt($(val).data("sid"))==parseInt(sid);
        })

        // console.log(slt)
        mLoading.start()
        Aws.uploadFileAWS($("#sUpload")[0].files[0],function (res,file) {
            if (res!="error"){
                console.log(res)
                $($(slt).find(".filename")[0]).val(file.name);
                $($(slt).find(".mtag")[0]).attr("src",res);

                var filename=file.name;
                var pagenum=$(slt).find(".pagenum")[0].value;
                var brief=$(slt).find(".brief")[0].value;
                var seq=$(slt).find(".seq")[0].value;
                var type=$($(slt).find(".slttype")[0]).val();
                var url=res;
                var html=pimgHtml({"bookid":id,"filename":filename,"pagenum":pagenum,"brief":brief,"seq":seq,"type":type,"url":url,"id":sid})
                $(slt).replaceWith(html);
                mLoading.complete()
            }
        })
    }
    else {
        mDialog.Check("請選取圖片")
    }
});

function triggerUpload(id) {
    $("#pUpload").removeClass("active");
    $("#sUpload").addClass("active");
    $("#fUpload").trigger("click")
}
var vfmt=["mp4","mpeg","wmv","m4v","avi","ogm","webm","asx","mov"];
var afmt=["mp3","m4a","wma","flac","ogg","acc","mpeg","wav"];
function gettagdata(url) {
    var tmp=url.split('.');
    var ext=tmp[tmp.length-1];
    if (vfmt.indexOf(ext)>-1)
        return ["video",ext];
    else if (afmt.indexOf(ext)>-1)
        return ["audio",ext];
    else
        return ["img",ext];
}
function setPimgs(imgs) {
    saveJson(imgs);
    var img="";
    $(imgs).each(function (idx,val) {
        img+=pimgHtml(val);
    })
    img+='<div id="pUpload" onclick="triggerUpload(0)" class="pointer text-center" ' +
        'style="position: relative;margin-right:28px;margin-bottom:28px;background:lightgray;width:216px;height:auto">' +
        '<img class="img-responsive" src="imgs/ic_update.png" style="width:40px;margin: 0 auto;margin-top:30px;margin-bottom:10px">' +
        '<label>新增影音檔</label></div>';
    $("#divPimg").html(img);
}

function pimgHtml(val) {
    // var tag=(isvideo(val.url))?"video":"audio";
    // var type=(isvideo(val.url))?'type = "video/mp4"':'type = "audio/wav"';
    var tmpdata=gettagdata(val.url);
    var media="";
    if (val.url==null){
        media='<img src="imgs/NoMedia.png"  style="width:170px;height:180px"  />';
    }
    else{
        media='<'+tmpdata[0]+' class="mtag" src="'+val.url+'" type="'+tmpdata[0]+'/'+tmpdata[1]+
            '"  style="width:100%;height:150px" controls />';
        //'" onerror="this.src = \'imgs/NoImage.png\';" style="width:100%;height:150px" controls />';
    }
    var select1=(val.type=="T")?" selected":"";
    var select2=(val.type=="P")?" selected":"";
    return '<div class="pimg w3-quarter" data-sid="'+val.id+'" style="position: relative;margin-right:28px;margin-bottom:28px;background:lightgray;">'+
            media+
        // '<div><'+tag+' src="'+val.url+'" '+type+' onerror="this.src = \'imgs/NoImage.png\';" style="width:170px;height:180px" controls ></div>' +
        '<img class="pointer trash" src="imgs/ic_delete.png" style="position: absolute; top: 25px; left: 25px;" />'+
        '<img class="pointer supload" src="imgs/ic_update_0.png" style="position: absolute; top: 25px; right: 25px;background-color: white" onclick="singleUpload('+val.id+')" />'+
        '<div class="tb-content overflow">'+
        '<label>頁碼</label>'+
        '<input class="pagenum" style="width:170px;margin-left:5px" value="'+val.pagenum+'" />'+
        '</div>'+
        '<div class="tb-content overflow">'+
        '<label>檔名</label>'+
        '<input class="filename" style="width:170px;margin-left:5px" value="'+val.filename+'" />'+
        '</div>'+
        '<div class="tb-content overflow">'+
        '<label>簡述</label>'+
        '<input class="brief" style="width:170px;margin-left:5px;" value="'+val.brief+'" />'+
        '<input class="seq" style="display:none" value="'+val.seq+'" />'+
        '</div>'+
        '<div class="tb-content overflow">'+
        '<label>適用</label>'+
        '<select class="slttype" style="width:170px;margin-left:5px;">'+
        '<option value="P" '+select2+'>家長</option>'+
        '<option value="T" '+select1+'>老師</option>'+
        '</select>'+
        '</div>'+
        '</div>';
}



//擷取網頁參數
function QueryString(AllVars, name)
{
    var Vars = AllVars.split("&");
    for (var i = 0; i < Vars.length; i++)
    {
        var Var = Vars[i].split("=");
        if (Var[0] == name) return Var[1];
    }
    return "";
}

var tmprec=[];
    // [{"id":1,"Iscover":"N","Url":"https://www.gravatar.com/avatar/17f5502f74c9d40b40cc238c9d0ca171?s=23&d=identicon&r=PG"},
    // {"id":2,"Iscover":"N","Url":"https://www.gravatar.com/avatar/17f5502f74c9d40b40cc238c9d0ca171?s=23&d=identicon&r=PG"},
    // {"id":3,"Iscover":"N","Url":"https://www.gravatar.com/avatar/17f5502f74c9d40b40cc238c9d0ca171?s=23&d=identicon&r=PG"},
    // {"id":4,"Iscover":"N","Url":"https://www.gravatar.com/avatar/17f5502f74c9d40b40cc238c9d0ca171?s=23&d=identicon&r=PG"},
    // {"id":5,"Iscover":"N","Url":"https://www.gravatar.com/avatar/17f5502f74c9d40b40cc238c9d0ca171?s=23&d=identicon&r=PG"},
    // {"id":6,"Iscover":"N","Url":"https://www.gravatar.com/avatar/17f5502f74c9d40b40cc238c9d0ca171?s=23&d=identicon&r=PG"}]
var id = QueryString(window.location.search.substring(1), "id");
$(function () {
    //init localstorage
    localStorage["jsonData"]=[];
    // console.log("***id***:"+id);
    setSelecttype(id);
    if (id!=""){
        WebAPI.getMedialist(id,function (data) {
            setPimgs(data.data);
        })
    }
    else
        setPimgs(tmprec);

    $(".imgrow").on("click",".trash",function () {
      var div = $(this).parent('div');
      mDialog.Confirm("你確定要刪除此影音資料","請確認",function (ok) {
        if (ok) {
          ok.close();
          $(div).remove();
        }
      })
    })

    $("#btnSave").click(function () {
        if (id==null ||id==""){
            mDialog.Check("no id");
            return;
        }

        //
        var tmp=$(".pimg");
        var medias=[];
        if (tmp.length==0){
            mDialog.Check("尚未設定影音資料");
            return;
        }
        for (var i=0;i<tmp.length;++i){
            var val=tmp[i]
            var filename=$(val).find(".filename")[0].value;
            var pagenum=$(val).find(".pagenum")[0].value;
            var brief=$(val).find(".brief")[0].value;
            var seq=$(val).find(".seq")[0].value;
            var type=$($(val).find(".slttype")[0]).val();
            var url=$($(val).find(".mtag")[0]).attr("src");
            if (pagenum==""){
                mDialog.Check("必須輸入頁碼");
                return;
            }
            else{
                if (seq=="" || seq==null || seq=="null"){
                    seq=getSeq(medias,pagenum);
                }
                medias.push({"bookid":id,"filename":filename,"pagenum":pagenum,"brief":brief,"seq":seq,"type":type,"url":url});
            }
        }

        WebAPI.setBookmedia(medias,id,function (data) {
            if (data.code=="0") {
                mDialog.Check("儲存作業成功","",function () {
                  window.history.back()
                });
            }
            else{
                mDialog.Check("儲存設定作業失敗:"+data.responseJSON.message);
            }
        })
    })


    $("#sltBook").on("change", function() {
        id= $(this).find(":selected").val() ;
        window.location.href="videocontent?id="+id;
    });

    $("#btnOrder").click(function () {
        var sortdata=sortJson($("#sltOrder").val());
        setPimgs(sortdata);
    })
})

