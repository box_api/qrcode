function login(act, pwd) {
    if (!pwd) return mDialog.Check('密碼不可為空');

    WebAPI.loginAdmin(act,pwd, function (data) {
        console.log(data)
        if (data.status == 200){
            sessionStorage['id'] = data.id;
            sessionStorage['account'] = data.account;
            window.location.href = 'booklist';
        }else {
            mDialog.Check(data.responseJSON.message);
        }
    });

}

$(function () {
    if (sessionStorage.length > 0){
        sessionStorage.clear();
    }

    $('.btn-warning').click(function () {
        var act = $('#inputAct').val(), pwd = $('#inputPwd').val();
        login(act, pwd);
    });

    $('body').keypress(function (e) {
        if (e.keyCode == 13){
            $('.btn-warning').trigger('click');
        }
    });
});