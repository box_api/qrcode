$.fn.datepicker.dates['custom'] = {
    days: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
    daysShort: ["日","一","二","三","四","五","六"],
    daysMin: ["日","一","二","三","四","五","六"],
    months: ["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
    monthsShort: ["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
    today: "今天",
    clear: "Clear",
    format: "yyyy/mm/dd",
    titleFormat: "yyyy年 MM",
    weekStart: 6,
    leftArrow: '<i class="fa fa-long-arrow-left"></i>',
    rightArrow: '<i class="fa fa-long-arrow-right"></i>'
};
